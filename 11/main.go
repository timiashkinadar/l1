package main

import "fmt"

func findCross(e1 []int, e2 []int) []int {
	m := make(map[int]bool)
	var res []int // Создаем пустой слайс для хранения пересекающихся элементов
	for _, i := range e1 {
		m[i] = true // Добавляем элементы первого списка в карту
	}

	for _, i := range e2 {
		if m[i] { // Проверяем, если элемент присутствует в карте
			res = append(res, i) // Добавляем пересекающийся элемент в слайс
		}
	}
	return res
}

func main() {
	elems1 := []int{2, 3, 9, 8}
	elems2 := []int{1, 3, 2, 9}

	fmt.Println(findCross(elems1, elems2))
}
