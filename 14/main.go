package main

import "fmt"

func main() {
	var x interface{} = true

	switch v := x.(type) { // Использование оператора switch для определения типа переменной x
	case nil: //В случае, если тип переменной x является nil
		fmt.Println("x is nil")
	case int: //В случае, если тип переменной x является int
		fmt.Println("x is int", v)
	case bool: //В случае, если тип переменной x является bool
		fmt.Println("x is bool", v)
	case string: //В случае, если тип переменной x является string
		fmt.Println("x is string", v)
	case chan string: //В случае, если тип переменной x является chan string
		fmt.Println("x is chan string", v)
	case chan int: //В случае, если тип переменной x является chan int
		fmt.Println("x is chan int", v)
	default: //В случае, если тип переменной x не совпадает с предыдущими случаями
		fmt.Println("type unknown")
	}
}
