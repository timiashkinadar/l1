package main

import (
	"fmt"
	"time"
)

func main() {

	ticker := time.NewTicker(time.Second * 6) // Создаем таймер, срабатывающий каждые 6 секунд
	ch := make(chan int)

	go func() {
		for i := 1; i < 10000; i++ {
			ch <- i // Помещаем числа в канал
			time.Sleep(time.Second)
		}
	}()

	for {
		select {
		case v := <-ch:
			fmt.Println(v) // Выводим полученные значения из канала
		case <-ticker.C:
			fmt.Println("Time is over!") // Выводим сообщение о завершении времени
			close(ch)                    // Закрываем канал
			return
		}
	}
}
