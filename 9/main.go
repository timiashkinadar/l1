package main

import "fmt"

func main() {

	nums := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}

	ch1 := make(chan int) // Создаем канал 1 для передачи чисел
	ch2 := make(chan int) // Создаем канал 2 для передачи удвоенных чисел

	go func() {
		for _, num := range nums {
			ch1 <- num // Помещаем числа в канал 1
		}
		close(ch1) // Закрываем канал 1 после передачи всех чисел
	}()

	go func() {
		for c := range ch1 {
			ch2 <- c * 2 // Удваиваем числа и помещаем их в канал 2
		}
		close(ch2) // Закрываем канал 2 после передачи всех удвоенных чисел
	}()

	for i := range ch2 {
		fmt.Println(i) // Выводим числа из канала 2
	}
}
