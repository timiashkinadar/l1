package main

var justString string

func someFunc() {
	v := createHugeString(1 << 10)
	justStringByte := make([]byte, 100) //Создаем слайс байтов с размером 100
	copy(justStringByte, v[:100])       //Копируем 100 байт в justString
	justString = string(justStringByte)
	v = nil // Освобождение памяти путем установки переменной v в nil

}

func createHugeString(i int) []byte {
	return make([]byte, i)
}

func main() {
	someFunc()
}
