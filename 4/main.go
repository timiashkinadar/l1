package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func Worker(ctx context.Context, input <-chan interface{}, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		select {
		case data := <-input:
			fmt.Println(data)
		case <-ctx.Done():
			// Воркер завершил работу
			return
		}
	}
}

func main() {
	numWorkers := 5
	input := make(chan interface{})
	ctx, cancel := context.WithCancel(context.Background())

	// Создание и запуск воркеров
	var wg sync.WaitGroup
	wg.Add(numWorkers)

	// Запуск воркеров
	for i := 1; i <= numWorkers; i++ {
		go Worker(ctx, input, &wg)
	}

	// Постоянная запись данных в канал
	go func() {
		for i := 1; ; i++ {
			input <- i
			time.Sleep(time.Millisecond * 100)
		}
	}()

	go func() {
		sigCh := make(chan os.Signal, 1)
		signal.Notify(sigCh, os.Interrupt, syscall.SIGTERM)
		<-sigCh
		fmt.Println("\nCtrl + C signal detected.\nCancelling context")

		// Завершение воркеров
		cancel()
	}()

	// Ожидание завершения всех воркеров
	wg.Wait()

	fmt.Println("All workers have finished")
}
