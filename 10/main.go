package main

import (
	"fmt"
	"math"
)

func main() {
	temp := []float64{-25.4, -27.0, 13.0, 19.0, 15.5, 24.5, -21.0, 32.5}

	m := make(map[int][]float64) // Создаем карту для группировки температур по диапазонам

	for _, f := range temp {
		r := int(math.Floor(f/10.0)) * 10 // Вычисляем диапазон для текущей температуры
		if r < 0 {
			r += +10 // Корректируем диапазон для отрицательных значений
		}
		m[r] = append(m[r], f) // Добавляем температуру в соответствующий диапазон в карте
	}

	for k, v := range m {
		fmt.Printf("%d:%v\n", k, v) // Выводим диапазон и соответствующие ему температуры
	}
}
