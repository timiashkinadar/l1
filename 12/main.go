package main

import "fmt"

func main() {
	strs := []string{"cat", "cat", "dog", "cat", "tree"}

	m := make(map[string]bool) // Создание карты для хранения уникальных значений

	for _, s := range strs {
		m[s] = true // Добавление каждой строки в карту
	}

	for k := range m { // Итерация по уникальным значениям
		fmt.Println(k)
	}
}
