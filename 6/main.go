package main

import (
	"context"
	"fmt"
	"sync"
)

func main() {

	// 1 СПОСОБ ОСТАНОВИТЬ ГОРУТИНУ:
	// отправлять сигнал в специальный канал,
	// а в горутине завершать работу при получении сигнала:
	stop := make(chan bool)
	go func() {
		for {
			select {
			case <-stop:
				return
			default:
				fmt.Println("action")
			}
		}
	}()

	// остановить горутину:
	stop <- true

	// 2 СПОСОБ ОСТАНОВИТЬ ГОРУТИНУ:
	// отменять контекст,
	// а в горутине завершать работу при отмене контекста:

	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			default:
				fmt.Println("action")
			}
		}
	}()

	// остановить горутину:
	cancel()

	// 3 СПОСОБ ОСТАНОВИТЬ ГОРУТИНУ:
	// использовать sync.WaitGroup:

	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			fmt.Println("action")
		}
	}()

	// остановить горутину:
	wg.Wait()
}
