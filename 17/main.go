package main

import "fmt"

func binarySearch(i int, nums []int) bool {

	low := 0
	high := len(nums) - 1

	for low <= high {
		median := (low + high) / 2 // Вычисление среднего индекса

		if nums[median] < i {
			low = median + 1 // Если значение по среднему индексу меньше искомого числа, сдвигаем нижнюю границу
		} else {
			high = median - 1 // Если значение по среднему индексу больше или равно искомому числу, сдвигаем верхнюю границу
		}
	}

	if low == len(nums) || nums[low] != i {
		return false // Если нижняя граница выходит за пределы массива или элемент по нижней границе не равен искомому числу, возвращаем false
	}

	return true
}

func main() {
	nums := []int{2, 4, 5, 8, 9, 11, 14, 18}
	fmt.Println(binarySearch(6, nums))
}
