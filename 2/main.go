package main

import (
	"fmt"
	"sync"
)

func squareNumber(ch chan int, wg *sync.WaitGroup) {
	defer wg.Done() //Уменьшаем счетчик ожидаемых горутин при завершении функции
	n := <-ch
	fmt.Println(n * n)
}

func main() {
	var m = []int{2, 4, 6, 8, 10}

	ch := make(chan int)

	wg := sync.WaitGroup{} // Создаем WaitGroup для ожидания завершения всех горутин

	for i := 0; i < len(m); i++ {
		wg.Add(1)                //Увеличиваем счетчик ожидаемых горутин на 1
		go squareNumber(ch, &wg) //Запускаем горутину для обработки числа
		ch <- m[i]
	}

	wg.Wait() // Ожидаем завершения всех горутин
}
