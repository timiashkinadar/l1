package main

import "fmt"

func reverseString(str string) string {
	r := []rune(str) // Преобразование строки в слайс рун
	for i, j := 0, len(r)-1; i < j; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i] // Обмен значений между парами символов от начала и конца слайса
	}
	return string(r)
}

func main() {
	str := "abcdefg"
	res := reverseString(str)
	fmt.Println(res)
}
