package main

import (
	"fmt"
	"sync"
)

func main() {
	var res int

	nums := []int{2, 4, 6, 8, 10}

	ch := make(chan int)

	wg := sync.WaitGroup{}

	// Запускаем горутины для обработки каждого числа из слайса
	for _, num := range nums {
		wg.Add(1)
		go func(n int, ch chan int) {
			defer wg.Done()
			sq := n * n // Вычисляем квадрат числа
			ch <- sq    // Помещаем результат в канал
		}(num, ch)
	}

	// Горутина для ожидания завершения всех горутин и закрытия канала
	go func() {
		wg.Wait()
		close(ch)
	}()

	// Итерируемся по значениям из канала и суммируем их
	for i := range ch {
		res += i
	}

	fmt.Println(res)
}
