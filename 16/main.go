package main

import (
	"fmt"
	"math/rand"
)

func quickSort(n []int) []int {
	if len(n) <= 1 {
		return n
	}

	left, right := 0, len(n)-1

	pivot := rand.Int() % len(n) // Выбор случайного элемента в качестве опорного

	n[pivot], n[right] = n[right], n[pivot] // Перемещение опорного элемента в конец

	for i, _ := range n {
		if n[i] < n[right] {
			n[left], n[i] = n[i], n[left] // Перемещение элементов меньше опорного влево
			left++
		}
	}

	n[left], n[right] = n[right], n[left] // Перемещение опорного элемента на правильную позицию

	quickSort(n[:left])   // Рекурсивная сортировка левой части
	quickSort(n[left+1:]) // Рекурсивная сортировка правой части

	return n
}

func main() {
	nums := []int{2, 6, 1, 3, 5, 4, 9, 7}

	fmt.Println(quickSort(nums))

}
