package main

import "fmt"

// Интерфейс для британской розетки

type BritishSocket interface {
	PlugInUKDevice(device string) string
}

type UKSocket struct{}

func (s *UKSocket) PlugInUKDevice(device string) string {
	return "Plugged in UK device: " + device
}

// Интерфейс для европейской розетки

type EuropeanSocket interface {
	PlugInEuroDevice(device string) string
}

type EuroSocket struct{}

func (s *EuroSocket) PlugInEuroDevice(device string) string {
	return "Plugged in Euro device: " + device
}

// Адаптер для британской розетки, позволяющий использовать европейское устройство

type UKToEuroAdapter struct {
	BritishSocket
}

func (adapter *UKToEuroAdapter) PlugInEuroDevice(device string) string {
	// Используем метод британской розетки для подключения европейского устройства
	return adapter.PlugInUKDevice(device)
}

func main() {
	uk := &UKSocket{}

	adapter := &UKToEuroAdapter{uk}

	res := adapter.PlugInEuroDevice("Device")
	fmt.Println(res)
}
