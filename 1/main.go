package main

import "fmt"

type Human struct {
	Name string
	Age  int
}

type Action struct {
	Human
}

func (h *Human) name(name string) {
	fmt.Printf("My name is %s.", name)
}

func main() {
	a := Action{}    // Создание экземпляра структуры Action
	a.name("Hannah") // Вызов метода name() для экземпляра Action, который наследует метод из структуры Human
}
