package main

import "fmt"

func main() {
	i, y := 1, 5

	i, y = y, i // Обмен значениями переменных без использования временной переменной

	fmt.Println(i, y)
}
