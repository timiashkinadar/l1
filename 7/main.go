package main

import (
	"fmt"
	"sync"
)

func main() {
	mu := sync.Mutex{} // Создаем Mutex для обеспечения безопасности доступа к map

	wg := sync.WaitGroup{}
	m := make(map[int]int)
	mass := []int{1, 1, 1, 2, 2, 1, 3, 4, 2, 7, 3, 8, 9, 9, 9, 9, 2, 4, 4, 4, 4}

	wg.Add(len(mass)) // Устанавливаем счетчик ожидаемых горутин в значение длины слайса

	for _, j := range mass {
		go func(m map[int]int, k int) {
			defer wg.Done()
			mu.Lock()       // Захватываем Mutex для безопасного доступа к map
			m[k] = m[k] + 1 // Изменяем значение в map
			mu.Unlock()     // Освобождаем Mutex
		}(m, j)
	}

	wg.Wait() // Ожидаем завершения всех горутин

	fmt.Println(m)
}
