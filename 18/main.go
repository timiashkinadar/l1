package main

import (
	"fmt"
	"sync"
)

type Counter struct {
	value int        // Поле для хранения значения счетчика
	mu    sync.Mutex // Мьютекс для обеспечения потокобезопасности
}

func (c *Counter) Increment() {
	c.mu.Lock()   // Блокировка мьютекса для защиты доступа к переменной
	c.value += 1  // Инкремент значения счетчика
	c.mu.Unlock() // Разблокировка мьютекса после выполнения операции
}

func main() {
	counter := Counter{}
	wg := sync.WaitGroup{}

	for i := 0; i < 50; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			counter.Increment() // Инкрементирование счетчика внутри горутины
		}()
	}

	wg.Wait()
	fmt.Println(counter.value)
}
