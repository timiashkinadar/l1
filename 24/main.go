package main

import (
	"fmt"
	"math"
)

type Point struct {
	x float64
	y float64
}

// конструктор объекта Point.

func NewPoint(x float64, y float64) *Point {
	return &Point{x: x, y: y}
}

//вычисляем расстояние между текущей точкой и переданной точкой.

func (p *Point) Distant(point *Point) float64 {
	x := p.x - point.x
	y := p.y - point.y

	return math.Sqrt((x * x) + (y * y))
}

func main() {
	p := Point{
		x: 3.0,
		y: 3.0,
	}

	point := NewPoint(-1.0, 0.0)

	fmt.Println(point.Distant(&p))
}
