package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "abcderqa"

	fmt.Println(checkSymb(str))
}

func checkSymb(s string) bool {

	//приводим строку к единому регистру
	s = strings.ToLower(s)
	m := make(map[rune]int)

	//увеличиваем значения в карте m для каждого символа
	for _, i := range s {
		m[i] = m[i] + 1
	}

	//проверем, что каждый символ в мапе встречается равно 1 раз
	for _, v := range m {
		if v != 1 {
			return false
		}
	}
	return true
}
