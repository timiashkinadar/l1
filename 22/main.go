package main

import (
	"fmt"
	"math"
)

func multiply(a, b float64) float64 {
	return a * b
}

func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func subtract(a, b float64) float64 {
	return a - b
}

func main() {
	a := math.Pow(2, 20) + 5
	b := math.Pow(2, 20) + 1

	fmt.Println(multiply(a, b))
	fmt.Println(divide(a, b))
	fmt.Println(sum(a, b))
	fmt.Println(subtract(a, b))
}
