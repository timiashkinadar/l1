package main

import (
	"fmt"
	"strings"
)

func main() {
	str := "snow dog sun"
	splt := strings.Split(str, " ") // Разделение строки на подстроки по пробелу и сохранение в слайсе

	var s []string

	for i := len(splt) - 1; i >= 0; i-- {
		s = append(s, splt[i]) // Добавление подстрок в слайс в обратном порядке
	}

	newStr := strings.Join(s, " ") // Объединение подстрок в одну строку, разделенную пробелами
	fmt.Println(newStr)
}
