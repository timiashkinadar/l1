package main

import (
	"log"
	"time"
)

func mySleep(d time.Duration) {
	<-time.After(d) //ожидаем истечения времени d,
}

func main() {
	log.Println("Start")
	mySleep(2 * time.Second)
	log.Println("The End")
}
